#!bin/bash

N=3
for exchange in "binance" "coinbase" "bitfinex" ;do
(
while read pair ;do 

#open in screen or something
yes | jesse import-candles "$exchange" $pair 2000-02-09
done < pairs-"$exchange".txt
) &
    if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
        wait -n
    fi

done

# wait for pending jobs
wait

# binance has missing candles on 2018-02-08 so we will append after that date.
while read pair
do yes | jesse import-candles binance $pair 2018-02-09
done < pairs-binance.txt
